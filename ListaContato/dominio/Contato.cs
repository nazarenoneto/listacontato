﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaContato.dominio
{
    class Contato
    {
        public string nome { get; set; }
        public string telefone { get; set; }
        public string email { get; set; }
        public string endereco { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaContato.dominio
{
    class Banco
    {
        private static List<Contato> dados;
        public static List<Contato> Dados
        {
            get
            {
                if (dados == null)
                    dados = new List<Contato>();
                return dados;
            }
            set
            {
                dados = value;
            }
        }        
    }
}
